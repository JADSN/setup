use crate::error::AppError;

use axum::{
    async_trait,
    extract::{rejection::JsonRejection, FromRequest, RequestParts},
    http::StatusCode,
    BoxError,
};
use serde::de::DeserializeOwned;

pub struct JsonExt<T>(pub T);

#[async_trait]
impl<B, T> FromRequest<B> for JsonExt<T>
where
    T: DeserializeOwned,
    B: axum::body::HttpBody + Send,
    B::Data: Send,
    B::Error: Into<BoxError>,
{
    type Rejection = (StatusCode, AppError);

    async fn from_request(req: &mut RequestParts<B>) -> Result<Self, Self::Rejection> {
        match axum::Json::<T>::from_request(req).await {
            Ok(value) => Ok(Self(value.0)),
            Err(rejection) => {
                let (status, body): (_, AppError) = match rejection {
                    JsonRejection::JsonDataError(err) => {
                        tracing::debug!("{err}");
                        (StatusCode::CONFLICT, AppError::InvalidInput)
                    }
                    JsonRejection::MissingJsonContentType(err) => {
                        tracing::debug!("{err}");

                        (StatusCode::CONFLICT, AppError::InvalidInput)
                    }
                    err => {
                        tracing::debug!("{err}");
                        (StatusCode::CONFLICT, AppError::InvalidInput)
                    }
                };

                Err((
                    status,
                    // we use `axum::Json` here to generate a JSON response
                    // body but you can use whatever response you want
                    body,
                ))
            }
        }
    }
}
