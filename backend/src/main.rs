mod api_router;
mod entrypoint;
mod error;
mod extractors;
mod mvp;

use std::process::ExitCode;

// Implementação do AppError para o ExitCode --> From
#[tokio::main]
async fn main() -> ExitCode {
    if let Err(error) = entrypoint::Dispatcher::dispatch().await {
        tracing::error!("[SERVER]: {error}");
        return ExitCode::FAILURE;
    }

    ExitCode::SUCCESS
}
