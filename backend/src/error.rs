use std::{fmt::Display, num::TryFromIntError};

use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use serde::Serialize;

// #[derive(Debug, thiserror::Error, Serialize)]
// pub enum ResponseError {
//     #[error("Unknown")]
//     Unknown,
// }

// #[derive(Debug, Serialize)]
// #[serde(rename_all = "camelCase")]
// #[serde(bound = "T: Serialize")]
// pub enum ResponseOrError<T> {
//     Success(T),
//     Error(ResponseError),
// }

#[derive(Debug, Serialize)]
pub struct ErrorMessage {
    message: String,
}

impl ErrorMessage {
    pub fn new(message: String) -> Self {
        Self { message }
    }
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        let (status, error_message) = match self {
            AppError::InvalidInput => (StatusCode::CONFLICT, self.to_string()),
            AppError::SqlxError(_) => (StatusCode::CONFLICT, self.to_string()),
            AppError::TryFromIntError(_) => (StatusCode::CONFLICT, self.to_string()),
            AppError::NotFound => (StatusCode::CONFLICT, self.to_string()),
        };

        let body = Json(ErrorMessage::new(error_message.to_string()));

        (status, body).into_response()
    }
}

#[derive(Debug, thiserror::Error)]
pub enum AppError {
    SqlxError(#[from] sqlx::Error),
    TryFromIntError(#[from] TryFromIntError),
    NotFound,
    InvalidInput,
}

impl Into<StatusCode> for AppError {
    fn into(self) -> StatusCode {
        match self {
            AppError::InvalidInput | AppError::NotFound => StatusCode::CONFLICT,
            AppError::TryFromIntError(error) => StatusCode::CONFLICT,
            AppError::SqlxError(error) => StatusCode::CONFLICT,
        }
    }
}

impl Display for AppError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let msg: &'static str = match self {
            AppError::InvalidInput => "Invalid input",
            AppError::NotFound => "Not Found",
            AppError::TryFromIntError(error) => {
                tracing::warn!("Conversion error. Reason: {error:?}");
                "Conversion Error".into()
            }
            AppError::SqlxError(error) => match &*error {
                // sqlx::Error::Configuration(_) => todo!(),
                // sqlx::Error::Database(_) => todo!(),
                // sqlx::Error::Io(_) => todo!(),
                // sqlx::Error::Tls(_) => todo!(),
                // sqlx::Error::Protocol(_) => todo!(),
                // sqlx::Error::RowNotFound => todo!(),
                // sqlx::Error::TypeNotFound { type_name } => todo!(),
                // sqlx::Error::ColumnIndexOutOfBounds { index, len } => todo!(),
                // sqlx::Error::ColumnNotFound(_) => todo!(),
                // sqlx::Error::ColumnDecode { index, source } => todo!(),
                // sqlx::Error::Decode(_) => todo!(),
                // sqlx::Error::PoolTimedOut => todo!(),
                // sqlx::Error::PoolClosed => todo!(),
                // sqlx::Error::WorkerCrashed => todo!(),
                // sqlx::Error::Migrate(_) => todo!(),
                error => {
                    tracing::warn!("Error one database. Reason: {error:?}");
                    "Database Error".into()
                }
            },
        };

        write!(f, "{}", msg)
    }
}
