use serde::{de::DeserializeOwned, Serialize};

#[async_trait::async_trait]
pub trait Mvp<DBCONNECTION, PAYLOAD, PATH, OUTCOME, ERROR>: Sync + Send + Sized {
    type Repository: Sync + Send + Sized;

    async fn model(
        &self,
        payload: PAYLOAD,
        path: PATH,
        pool_connection: DBCONNECTION,
        repository: &Self::Repository,
    ) -> Result<OUTCOME, ERROR>
    where
        PAYLOAD: 'async_trait + DeserializeOwned + Send + Sync,
        PATH: 'async_trait + Send + Sync,
        OUTCOME: 'async_trait + Serialize + Send + Sync,
        DBCONNECTION: 'async_trait + Send + Sync,
        Self: 'async_trait + Send + Sync;

    async fn view(&self, payload: Result<OUTCOME, ERROR>) -> Result<OUTCOME, ERROR>
    where
        OUTCOME: 'async_trait + Serialize + Send + Sync,
        ERROR: 'async_trait + Send + Sync;

    async fn presenter(
        &self,
        pool_connection: DBCONNECTION,
        payload: PAYLOAD,
        path: PATH,
        repository: &Self::Repository,
    ) -> Result<OUTCOME, ERROR>
    where
        PAYLOAD: 'async_trait + DeserializeOwned + Send + Sync,
        PATH: 'async_trait + Send + Sync,
        DBCONNECTION: 'async_trait + Send + Sync,
        OUTCOME: 'async_trait + Serialize + Send + Sync,
        ERROR: 'async_trait + Send + Sync,
        Self: 'async_trait + Send + Sync,
    {
        let model_data = self.model(payload, path, pool_connection, repository).await;
        let view_data = self.view(model_data).await;
        view_data
    }
}
