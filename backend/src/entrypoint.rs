use std::net::SocketAddr;

use axum::{
    http::{header, HeaderValue, Request},
    routing::get,
    Extension, Router,
};
use sqlx::postgres::PgPoolOptions;
use tokio::time::Duration;
use tower::ServiceBuilder;
use tower_http::{
    classify::ServerErrorsFailureClass, cors::CorsLayer, set_header::SetResponseHeaderLayer,
    timeout::TimeoutLayer, trace::TraceLayer,
};
use tracing::Span;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

pub struct Dispatcher;

impl Dispatcher {
    pub async fn dispatch() -> anyhow::Result<()> {
        tracing_subscriber::registry()
            .with(tracing_subscriber::EnvFilter::new(
                std::env::var("RUST_LOG")
                    .unwrap_or_else(|_| "backend=debug,tower_http=debug".into()),
            ))
            .with(
                tracing_subscriber::fmt::layer()
                    .with_file(true)
                    .with_line_number(true),
            )
            .init();

        #[cfg(debug_assertions)]
        let database_uri = env!("DATABASE_URL");

        #[cfg(not(debug_assertions))]
        let database_uri = env!("DB_PG_URL");

        let pool_connection = PgPoolOptions::new()
            .min_connections(1)
            .max_connections(1)
            .connect(database_uri)
            .await?;

        let bootstrap_query = include_str!("../migrations/20220617201517_initial_migration.sql");

        sqlx::query(bootstrap_query)
            .execute_many(&pool_connection)
            .await;
        // .map_err(|error| {
        //     tracing::debug!("[ERROR]: {error}");
        //     error
        // })?;

        let routes = Router::new().merge(super::api_router::dispatcher::Dispatcher::dispatch());

        let app = Router::new()
            .route("/", get(root))
            .nest("/api", routes)
            .layer(
                TraceLayer::new_for_http()
                    .on_request(|request: &Request<_>, _span: &Span| {
                        let method = request.method().to_string();
                        let uri = request.uri().to_string();

                        tracing::debug!("[SERVER] {method}:{uri}")
                    })
                    // .on_response(|_response: &Response, _latency: Duration, _span: &Span| {})
                    // .on_body_chunk(|_chunk: &Bytes, _latency: Duration, _span: &Span| {})
                    // .on_eos(
                    //     |_trailers: Option<&HeaderMap>,
                    //      _stream_duration: Duration,
                    //      _span: &Span| {},
                    // )
                    .on_failure(
                        |error: ServerErrorsFailureClass, _latency: Duration, _span: &Span| {
                            tracing::error!("[SERVER] {error}")
                        },
                    ),
            )
            .layer(
                ServiceBuilder::new()
                    .layer(CorsLayer::permissive())
                    .layer(Extension(pool_connection))
                    .layer(TimeoutLayer::new(Duration::from_secs(5)))
                    .layer(SetResponseHeaderLayer::if_not_present(
                        header::USER_AGENT,
                        HeaderValue::from_static("Bozo"),
                    )),
            );

        let addr = SocketAddr::from(([0, 0, 0, 0], 3000));
        tracing::debug!("Listening on http://{}", addr);
        axum::Server::bind(&addr)
            .serve(app.into_make_service())
            .await
            .map_err(|error| {
                tracing::debug!("[ERROR]: {error}");
                error
            })?;

        Ok(())
    }
}

async fn root() -> &'static str {
    // sleep(Duration::from_secs(5)).await; // Timeout
    "Root"
}
