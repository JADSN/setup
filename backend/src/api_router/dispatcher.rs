use axum::Router;

pub struct Dispatcher;

impl Dispatcher {
    pub fn dispatch() -> Router {
        Router::new().merge(crate::api_router::permissions_route::dispatch::dispatch())
    }
}
