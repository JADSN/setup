use sqlx::PgPool;

use super::types::{ArcDynTaskRepositoryReadOne, ReadOneMvp};
use crate::{api_router::permissions_route::entities::ReadOneEntity, error::AppError, mvp::Mvp};

#[async_trait::async_trait]
impl Mvp<PgPool, (), u16, ReadOneEntity, AppError> for ReadOneMvp {
    type Repository = ArcDynTaskRepositoryReadOne;
    //* VALIDATION AND DATABASE IMPLEMENTATION */
    async fn model(
        &self,
        _payload: (),
        path: u16,
        pool_connection: PgPool,
        repository: &Self::Repository,
    ) -> Result<ReadOneEntity, AppError> {
        repository.read_one(pool_connection, path).await
    }

    //* NORMALIZE RESPONSE */
    async fn view(
        &self,
        payload: Result<ReadOneEntity, AppError>,
    ) -> Result<ReadOneEntity, AppError> {
        payload
    }
}
