use std::sync::Arc;

use crate::{api_router::permissions_route::entities::ReadOneEntity, error::AppError};

#[derive(Debug, Clone)]
pub struct ReadOneMvp;

pub struct TaskRepositoryReadOne;
pub type ArcDynTaskRepositoryReadOne = Arc<dyn ITaskRepositoryReadOne + Send + Sync>;

#[async_trait::async_trait]
pub trait ITaskRepositoryReadOne {
    async fn read_one(
        &self,
        pool_connection: sqlx::PgPool,
        identifier: u16,
    ) -> Result<ReadOneEntity, AppError>;
}
