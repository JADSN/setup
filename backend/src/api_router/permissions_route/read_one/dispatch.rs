use axum::{extract::Path, Extension, Json};

use super::types::{ArcDynTaskRepositoryReadOne, ReadOneMvp};
use crate::{api_router::permissions_route::entities::ReadOneEntity, error::AppError, mvp::Mvp};

pub async fn dispatch(
    Extension(mvp): Extension<ReadOneMvp>,
    Extension(pool_connection): Extension<sqlx::PgPool>,
    Extension(repository): Extension<ArcDynTaskRepositoryReadOne>,
    Path(identifier): Path<u16>,
) -> Result<Json<ReadOneEntity>, AppError> {
    match mvp
        .presenter(pool_connection, (), identifier, &repository)
        .await
    {
        Ok(data) => Ok(Json(data)),
        Err(error) => Err(error),
    }
}
