use super::types::{ITaskRepositoryReadOne, TaskRepositoryReadOne};
use crate::{api_router::permissions_route::entities::ReadOneEntity, error::AppError};

//* MODEL DATABASE IMPLEMENTATION */
#[async_trait::async_trait]
impl ITaskRepositoryReadOne for TaskRepositoryReadOne {
    async fn read_one(
        &self,
        pool_connection: sqlx::PgPool,
        identifier: u16,
    ) -> Result<ReadOneEntity, AppError> {
        let mut transaction = pool_connection.begin().await?;

        let identifier_parsed = i16::try_from(identifier)?;

        let result_query = sqlx::query!(
            "SELECT id, name FROM permissions WHERE id=$1",
            identifier_parsed
        )
        .fetch_optional(&mut transaction)
        .await?;

        transaction.commit().await?;

        if let Some(record) = result_query {
            return Ok(ReadOneEntity::new(
                record.id.try_into().unwrap_or(0),
                record.name,
            ));
        } else {
            return Err(AppError::NotFound);
        }
    }
}
