use crate::{
    api_router::permissions_route::read_one::types::{ArcDynTaskRepositoryReadOne, ReadOneMvp},
    error::AppError,
    mvp::Mvp,
};

use super::types::{ArcDynTaskRepositoryDeleteOne, DeleteOneMvp};
use axum::{extract::Path, Extension, Json};

pub async fn dispatch(
    Extension(pool_connection): Extension<sqlx::PgPool>,
    Extension(mvp_read_one): Extension<ReadOneMvp>,
    Extension(mvp_delete_one): Extension<DeleteOneMvp>,
    Extension(repository_read_one): Extension<ArcDynTaskRepositoryReadOne>,
    Extension(repository_delete_one): Extension<ArcDynTaskRepositoryDeleteOne>,
    Path(identifier): Path<u16>,
) -> Result<Json<u8>, AppError> {
    match mvp_read_one
        .presenter(
            pool_connection.clone(),
            (),
            identifier,
            &repository_read_one,
        )
        .await
    {
        Ok(_) => {
            match mvp_delete_one
                .presenter(pool_connection, (), identifier, &repository_delete_one)
                .await
            {
                Ok(data) => Ok(Json(data)),
                Err(error) => Err(error),
            }
        }
        Err(error) => Err(error),
    }
}
