use sqlx::PgPool;

use super::types::{ArcDynTaskRepositoryDeleteOne, DeleteOneMvp};
use crate::{error::AppError, mvp::Mvp};

#[async_trait::async_trait]
impl Mvp<PgPool, (), u16, u8, AppError> for DeleteOneMvp {
    type Repository = ArcDynTaskRepositoryDeleteOne;
    //* VALIDATION AND DATABASE IMPLEMENTATION */
    async fn model(
        &self,
        _payload: (),
        path: u16,
        pool_connection: PgPool,
        repository: &Self::Repository,
    ) -> Result<u8, AppError> {
        repository.delete_one(pool_connection, path).await
    }

    //* NORMALIZE RESPONSE */
    async fn view(&self, payload: Result<u8, AppError>) -> Result<u8, AppError> {
        payload
    }
}
