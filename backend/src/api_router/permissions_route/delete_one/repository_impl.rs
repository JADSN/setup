use async_trait::async_trait;
use sqlx::PgPool;

use crate::error::AppError;

use super::types::{ITaskRepositoryDeleteOne, TaskRepositoryDeleteOne};

//* MODEL DATABASE IMPLEMENTATION */
#[async_trait]
impl ITaskRepositoryDeleteOne for TaskRepositoryDeleteOne {
    async fn delete_one(&self, pool_connection: PgPool, identifier: u16) -> Result<u8, AppError> {
        let mut transaction = pool_connection.begin().await?;

        let identifier_parsed = i16::try_from(identifier)?;

        let result_query = sqlx::query!("DELETE FROM permissions WHERE id=$1", identifier_parsed)
            .execute(&mut transaction)
            .await?;

        transaction.commit().await?;

        let affected_rows: u8 = result_query.rows_affected().try_into()?;

        Ok(affected_rows)
    }
}
