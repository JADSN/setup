use std::sync::Arc;

use crate::error::AppError;

#[derive(Debug, Clone)]
pub struct DeleteOneMvp;

pub struct TaskRepositoryDeleteOne;
pub type ArcDynTaskRepositoryDeleteOne = Arc<dyn ITaskRepositoryDeleteOne + Send + Sync>;

#[async_trait::async_trait]
pub trait ITaskRepositoryDeleteOne {
    async fn delete_one(
        &self,
        pool_connection: sqlx::PgPool,
        identifier: u16,
    ) -> Result<u8, AppError>;
}
