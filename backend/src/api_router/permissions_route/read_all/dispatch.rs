use axum::{Extension, Json};

use super::types::{ArcDynTaskRepositoryReadAll, ReadAllMvp};
use crate::{api_router::permissions_route::entities::ReadAllEntity, error::AppError, mvp::Mvp};

pub async fn dispatch(
    Extension(mvp): Extension<ReadAllMvp>,
    Extension(pool_connection): Extension<sqlx::PgPool>,
    Extension(repository): Extension<ArcDynTaskRepositoryReadAll>,
) -> Result<Json<Vec<ReadAllEntity>>, AppError> {
    match mvp.presenter(pool_connection, (), (), &repository).await {
        Ok(data) => Ok(Json(data)),
        Err(error) => Err(error),
    }
}
