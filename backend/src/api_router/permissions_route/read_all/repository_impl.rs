use super::types::{ITaskRepositoryReadAll, TaskRepositoryReadAll};
use crate::{api_router::permissions_route::entities::ReadAllEntity, error::AppError};

use sqlx::PgPool;

//* MODEL DATABASE IMPLEMENTATION */
#[async_trait::async_trait]
impl ITaskRepositoryReadAll for TaskRepositoryReadAll {
    async fn read_all(&self, pool_connection: PgPool) -> Result<Vec<ReadAllEntity>, AppError> {
        let mut transaction = pool_connection.begin().await?;

        //? Use query_as! and change the id  type
        let result_query = sqlx::query!("SELECT id, name from permissions")
            .fetch_all(&mut transaction)
            .await?;

        let result = result_query
            .into_iter()
            .map(|record| ReadAllEntity::new(record.id.try_into().unwrap_or(0), record.name))
            .collect::<Vec<_>>();

        transaction.commit().await?;

        Ok(result)
    }
}
