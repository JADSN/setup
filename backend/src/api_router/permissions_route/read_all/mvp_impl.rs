use async_trait::async_trait;
use sqlx::PgPool;

use crate::{api_router::permissions_route::entities::ReadAllEntity, error::AppError, mvp::Mvp};

use super::types::{ArcDynTaskRepositoryReadAll, ReadAllMvp};

#[async_trait]
impl Mvp<PgPool, (), (), Vec<ReadAllEntity>, AppError> for ReadAllMvp {
    type Repository = ArcDynTaskRepositoryReadAll;
    //* VALIDATION AND DATABASE IMPLEMENTATION */
    async fn model(
        &self,
        _payload: (),
        _path: (),
        pool_connection: PgPool,
        repository: &Self::Repository,
    ) -> Result<Vec<ReadAllEntity>, AppError> {
        repository.read_all(pool_connection).await
    }

    //* NORMALIZE RESPONSE */
    async fn view(
        &self,
        payload: Result<Vec<ReadAllEntity>, AppError>,
    ) -> Result<Vec<ReadAllEntity>, AppError> {
        payload
    }
}
