use std::sync::Arc;

use sqlx::PgPool;

use crate::{api_router::permissions_route::entities::ReadAllEntity, error::AppError};

//* READ COUNT */
#[derive(Debug, Clone)]
pub struct ReadAllMvp;

pub struct TaskRepositoryReadAll;
pub type ArcDynTaskRepositoryReadAll = Arc<dyn ITaskRepositoryReadAll + Send + Sync>;

#[async_trait::async_trait]
pub trait ITaskRepositoryReadAll {
    async fn read_all(&self, pool_connection: PgPool) -> Result<Vec<ReadAllEntity>, AppError>;
}
