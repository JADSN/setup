use serde::{Deserialize, Serialize};

use crate::error::AppError;

// use super::error::AppError;

//* READ ALL */
#[derive(Debug, Serialize)]
pub struct ReadAllEntity {
    #[serde(skip_serializing)]
    id: u64,
    name: String,
}

impl ReadAllEntity {
    pub fn new(id: u64, name: String) -> Self {
        Self { id, name }
    }
}

#[derive(Debug)]
struct ReadOnlyNamesentity {
    name: String,
}

#[derive(Debug, Serialize)]
pub struct ReadOneEntity {
    id: u64,
    name: String,
}

impl ReadOneEntity {
    pub fn new(id: u64, name: String) -> Self {
        Self { id, name }
    }
}

#[derive(Debug, Deserialize)]

//* CREATE */
pub struct CreateOneEntity {
    name: PermissionName,
}

impl CreateOneEntity {
    pub fn name(&self) -> &str {
        &self.name.take()
    }
}

//* UPDATE */
#[derive(Debug, Deserialize)]
pub struct UpdateOneEntity {
    // #[serde(skip_serializing_if = "Option::is_none")]
    name: Option<PermissionName>,
}

impl UpdateOneEntity {
    pub fn name(&self) -> Option<&PermissionName> {
        self.name.as_ref()
    }
}

#[derive(Debug, Deserialize)]
#[serde(try_from = "String")]
pub struct PermissionName(String);

impl PermissionName {
    pub fn take(&self) -> &str {
        self.0.as_str()
    }
}

impl TryFrom<String> for PermissionName {
    type Error = AppError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.is_empty() {
            // return Err(AppError::InvalidPermissionName);
            return Err(AppError::InvalidInput);
        }
        Ok(Self(value))
    }
}
