use std::sync::Arc;

use axum::{
    routing::{delete, get, head, patch, post},
    Extension, Router,
};

use crate::api_router::permissions_route::{
    create_one::types::{ArcDynTaskRepositoryCreateOne, CreateOneMvp, TaskRepositoryCreateOne},
    delete_one::types::{ArcDynTaskRepositoryDeleteOne, DeleteOneMvp, TaskRepositoryDeleteOne},
    read_all::types::{ArcDynTaskRepositoryReadAll, ReadAllMvp, TaskRepositoryReadAll},
    read_count::types::{ArcDynTaskRepositoryReadCount, ReadCountMvp, TaskRepositoryReadCount},
    read_one::types::{ArcDynTaskRepositoryReadOne, ReadOneMvp, TaskRepositoryReadOne},
    update_one::types::{ArcDynTaskRepositoryUpdateOne, TaskRepositoryUpdateOne, UpdateOneMvp},
};

pub fn dispatch() -> Router {
    const ROUTE_NAME: &str = "/permissions";

    //* READ COUNT REPOSITORY */
    let respository_read_count = Arc::new(TaskRepositoryReadCount) as ArcDynTaskRepositoryReadCount;
    let mvp_read_count = ReadCountMvp;

    //* READ ALL REPOSITORY */
    let respository_read_all = Arc::new(TaskRepositoryReadAll) as ArcDynTaskRepositoryReadAll;
    let mvp_read_all = ReadAllMvp;

    //* READ ONE REPOSITORY */
    let respository_read_one = Arc::new(TaskRepositoryReadOne) as ArcDynTaskRepositoryReadOne;
    let mvp_read_one = ReadOneMvp;

    //* CREATE ONE REPOSITORY */
    let respository_create_one = Arc::new(TaskRepositoryCreateOne) as ArcDynTaskRepositoryCreateOne;
    let mvp_create_one = CreateOneMvp;

    //* DELETE ONE REPOSITORY */
    let respository_delete_one = Arc::new(TaskRepositoryDeleteOne) as ArcDynTaskRepositoryDeleteOne;
    let mvp_delete_one = DeleteOneMvp;

    //* Update ONE REPOSITORY */
    let respository_update_one = Arc::new(TaskRepositoryUpdateOne) as ArcDynTaskRepositoryUpdateOne;
    let mvp_update_one = UpdateOneMvp;

    Router::new()
        .route(
            ROUTE_NAME,
            head(super::read_count::dispatch::dispatch)
                .layer(Extension(respository_read_count))
                .layer(Extension(mvp_read_count)),
        )
        .route(
            ROUTE_NAME,
            get(super::read_all::dispatch::dispatch)
                .layer(Extension(respository_read_all))
                .layer(Extension(mvp_read_all)),
        )
        .route(
            format!("{ROUTE_NAME}/:identifier").as_str(),
            get(super::read_one::dispatch::dispatch)
                .layer(Extension(respository_read_one.clone()))
                .layer(Extension(mvp_read_one.clone())),
        )
        .route(
            ROUTE_NAME,
            post(super::create_one::dispatch::dispatch)
                .layer(Extension(respository_create_one))
                .layer(Extension(mvp_create_one)),
        )
        .route(
            format!("{ROUTE_NAME}/:identifier").as_str(),
            delete(super::delete_one::dispatch::dispatch)
                .layer(Extension(respository_read_one))
                .layer(Extension(mvp_read_one))
                .layer(Extension(respository_delete_one))
                .layer(Extension(mvp_delete_one)),
        )
        .route(
            format!("{ROUTE_NAME}/:identifier").as_str(),
            patch(super::update_one::dispatch::dispatch)
                .layer(Extension(respository_update_one))
                .layer(Extension(mvp_update_one)),
        )
}
