use crate::error::AppError;

use super::types::{ITaskRepositoryReadCount, TaskRepositoryReadCount};

//* MODEL DATABASE IMPLEMENTATION */
#[async_trait::async_trait]
impl ITaskRepositoryReadCount for TaskRepositoryReadCount {
    async fn read_count(&self, pool_connection: sqlx::PgPool) -> Result<u64, AppError> {
        let mut transaction = pool_connection.begin().await?;

        let result_query = sqlx::query!("SELECT COUNT(*) from permissions")
            .fetch_optional(&mut transaction)
            .await?;

        let mut count = 0;
        if let Some(record) = result_query {
            let rows = record.count.unwrap().try_into().unwrap_or(0);
            count = rows;
        }

        transaction.commit().await?;

        Ok(count)
    }
}
