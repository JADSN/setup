use sqlx::PgPool;

use super::types::{ArcDynTaskRepositoryReadCount, ReadCountMvp};
use crate::{error::AppError, mvp::Mvp};

#[async_trait::async_trait]
impl Mvp<PgPool, (), (), u64, AppError> for ReadCountMvp {
    type Repository = ArcDynTaskRepositoryReadCount;
    //* VALIDATION AND DATABASE IMPLEMENTATION */
    async fn model(
        &self,
        _payload: (),
        _path: (),
        pool_connection: PgPool,
        repository: &Self::Repository,
    ) -> Result<u64, AppError> {
        repository.read_count(pool_connection).await
    }

    //* NORMALIZE RESPONSE */
    async fn view(&self, payload: Result<u64, AppError>) -> Result<u64, AppError> {
        payload
    }
}
