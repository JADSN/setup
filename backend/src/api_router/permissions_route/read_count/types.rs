use std::sync::Arc;

use crate::error::AppError;

#[derive(Debug, Clone)]
pub struct ReadCountMvp;

pub struct TaskRepositoryReadCount;
pub type ArcDynTaskRepositoryReadCount = Arc<dyn ITaskRepositoryReadCount + Send + Sync>;

#[async_trait::async_trait]
pub trait ITaskRepositoryReadCount {
    async fn read_count(&self, pool_connection: sqlx::PgPool) -> Result<u64, AppError>;
}
