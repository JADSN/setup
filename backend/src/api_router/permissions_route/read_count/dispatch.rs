use axum::{
    http::{header::HeaderName, HeaderMap, HeaderValue},
    Extension,
};

use super::types::{ArcDynTaskRepositoryReadCount, ReadCountMvp};
use crate::{error::AppError, mvp::Mvp};

pub async fn dispatch(
    Extension(mvp): Extension<ReadCountMvp>,
    Extension(pool_connection): Extension<sqlx::PgPool>,
    Extension(repository): Extension<ArcDynTaskRepositoryReadCount>,
) -> Result<HeaderMap, AppError> {
    match mvp.presenter(pool_connection, (), (), &repository).await {
        Ok(data) => {
            let mut headers = HeaderMap::new();
            headers.insert(HeaderName::from_static("x-count"), HeaderValue::from(data));

            Ok(headers)
        }
        Err(error) => Err(error),
    }
}
