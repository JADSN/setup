use std::sync::Arc;

use crate::{api_router::permissions_route::entities::CreateOneEntity, error::AppError};

#[derive(Debug, Clone)]
pub struct CreateOneMvp;

pub struct TaskRepositoryCreateOne;
pub type ArcDynTaskRepositoryCreateOne = Arc<dyn ITaskRepositoryCreateOne + Send + Sync>;

#[async_trait::async_trait]
pub trait ITaskRepositoryCreateOne {
    async fn crate_one(
        &self,
        pool_connection: sqlx::PgPool,
        payload: CreateOneEntity,
    ) -> Result<u8, AppError>;
}
