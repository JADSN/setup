use async_trait::async_trait;
use sqlx::PgPool;

use super::types::{ArcDynTaskRepositoryCreateOne, CreateOneMvp};
use crate::{api_router::permissions_route::entities::CreateOneEntity, error::AppError, mvp::Mvp};

#[async_trait]
impl Mvp<PgPool, CreateOneEntity, (), u8, AppError> for CreateOneMvp {
    type Repository = ArcDynTaskRepositoryCreateOne;
    //* VALIDATION AND DATABASE IMPLEMENTATION */
    async fn model(
        &self,
        payload: CreateOneEntity,
        _path: (),
        pool_connection: PgPool,
        repository: &Self::Repository,
    ) -> Result<u8, AppError> {
        repository.crate_one(pool_connection, payload).await
    }

    //* NORMALIZE RESPONSE */
    async fn view(&self, payload: Result<u8, AppError>) -> Result<u8, AppError> {
        payload
    }
}
