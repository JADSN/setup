use super::types::{ITaskRepositoryCreateOne, TaskRepositoryCreateOne};
use crate::{api_router::permissions_route::entities::CreateOneEntity, error::AppError};

//* MODEL DATABASE IMPLEMENTATION */
#[async_trait::async_trait]
impl ITaskRepositoryCreateOne for TaskRepositoryCreateOne {
    async fn crate_one(
        &self,
        pool_connection: sqlx::PgPool,
        payload: CreateOneEntity,
    ) -> Result<u8, AppError> {
        let mut transaction = pool_connection.begin().await?;

        let name = payload.name();

        let result_query = sqlx::query!("INSERT INTO PUBLIC.PERMISSIONS(name) VALUES ($1);", name)
            .execute(&mut transaction)
            .await?;

        transaction.commit().await?;

        let affected_rows: u8 = result_query.rows_affected().try_into()?;

        Ok(affected_rows)
    }
}
