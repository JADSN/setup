use axum::{Extension, Json};

use super::types::{ArcDynTaskRepositoryCreateOne, CreateOneMvp};
use crate::{
    api_router::permissions_route::entities::CreateOneEntity, error::AppError,
    extractors::json_ext::JsonExt, mvp::Mvp,
};

pub async fn dispatch(
    Extension(mvp): Extension<CreateOneMvp>,
    Extension(pool_connection): Extension<sqlx::PgPool>,
    Extension(repository): Extension<ArcDynTaskRepositoryCreateOne>,
    JsonExt(payload): JsonExt<CreateOneEntity>,
) -> Result<Json<u8>, AppError> {
    match mvp
        .presenter(pool_connection, payload, (), &repository)
        .await
    {
        Ok(data) => Ok(Json(data)),
        Err(error) => Err(error),
    }
}
