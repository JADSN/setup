use crate::{error::AppError, extractors::json_ext::JsonExt, mvp::Mvp};

use super::types::{ArcDynTaskRepositoryUpdateOne, UpdateOneMvp};
use axum::{extract::Path, Extension, Json};

use crate::api_router::permissions_route::entities::UpdateOneEntity;

pub async fn dispatch(
    Extension(mvp): Extension<UpdateOneMvp>,
    Extension(pool_connection): Extension<sqlx::PgPool>,
    Extension(repository): Extension<ArcDynTaskRepositoryUpdateOne>,
    JsonExt(payload): JsonExt<UpdateOneEntity>,
    Path(identifier): Path<u16>,
) -> Result<Json<u8>, AppError> {
    match mvp
        .presenter(pool_connection, payload, identifier, &repository)
        .await
    {
        Ok(data) => Ok(Json(data)),
        Err(error) => Err(error),
    }
}
