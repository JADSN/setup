use sqlx::PgPool;

use super::types::{ArcDynTaskRepositoryUpdateOne, UpdateOneMvp};
use crate::{api_router::permissions_route::entities::UpdateOneEntity, error::AppError, mvp::Mvp};

#[async_trait::async_trait]
impl Mvp<PgPool, UpdateOneEntity, u16, u8, AppError> for UpdateOneMvp {
    type Repository = ArcDynTaskRepositoryUpdateOne;
    //* VALIDATION AND DATABASE IMPLEMENTATION */
    async fn model(
        &self,
        payload: UpdateOneEntity,
        path: u16,
        pool_connection: PgPool,
        repository: &Self::Repository,
    ) -> Result<u8, AppError> {
        repository.update_one(pool_connection, payload, path).await
    }

    //* NORMALIZE RESPONSE */
    async fn view(&self, payload: Result<u8, AppError>) -> Result<u8, AppError> {
        payload
    }
}
