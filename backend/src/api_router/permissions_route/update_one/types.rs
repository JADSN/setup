use std::sync::Arc;

use crate::{api_router::permissions_route::entities::UpdateOneEntity, error::AppError};

#[derive(Debug, Clone)]
pub struct UpdateOneMvp;

pub struct TaskRepositoryUpdateOne;
pub type ArcDynTaskRepositoryUpdateOne = Arc<dyn ITaskRepositoryUpdateOne + Send + Sync>;

#[async_trait::async_trait]
pub trait ITaskRepositoryUpdateOne {
    async fn update_one(
        &self,
        pool_connection: sqlx::PgPool,
        payload: UpdateOneEntity,
        identifier: u16,
    ) -> Result<u8, AppError>;
}
