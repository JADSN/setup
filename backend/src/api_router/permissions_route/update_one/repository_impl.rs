use async_trait::async_trait;
use sqlx::PgPool;

use super::types::{ITaskRepositoryUpdateOne, TaskRepositoryUpdateOne};
use crate::{api_router::permissions_route::entities::UpdateOneEntity, error::AppError};

//* MODEL DATABASE IMPLEMENTATION */
#[async_trait]
impl ITaskRepositoryUpdateOne for TaskRepositoryUpdateOne {
    async fn update_one(
        &self,
        pool_connection: PgPool,
        payload: UpdateOneEntity,
        identifier: u16,
    ) -> Result<u8, AppError> {
        let mut transaction = pool_connection.begin().await?;

        let identifier_parsed = i16::try_from(identifier)?;

        let mut rows_affected: u8 = 0;
        if let Some(payload) = payload.name() {
            let name = payload.take();
            let result_query = sqlx::query!(
                "UPDATE PUBLIC.PERMISSIONS SET name=$1 WHERE id=$2;",
                name,
                identifier_parsed
            )
            .execute(&mut transaction)
            .await?;

            let result_query_rows_affected: u8 = result_query.rows_affected().try_into()?;

            rows_affected += result_query_rows_affected;
        }

        transaction.commit().await?;

        Ok(rows_affected)
    }
}
